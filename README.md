## KiCad Version Selector

This is a rust based tool meant to act as a file association "proxy".
The correct files (kicad_pcb,kicad_sch and kicad_pro) associated with this selector will result
in the selector finding the best matching KiCad version installed on your PC and launching it.

Best matching means:

- The generator_version is the exact version of a installed KiCad version
- Files with only "version" are opened by the oldest KiCad release that would have created them
- The newest KiCad version is used if no other match is found

In the event extracting version or KiCad installs fails, nothing will launch.

### Versioning

Use the version field in Cargo.toml

### Patches
cargo-patch is expected to be installed as a tool to assist in patching dependencies so we don't
need to maintain forks.

#### winreg
winreg needs to be patched to fix a bug where REG_NONE being encountered causes the entire deserialization process to be aborted.
