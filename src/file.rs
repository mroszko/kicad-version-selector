/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2024 Mark Roszko <mark.roszko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

use regex::Regex;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::path::PathBuf;
use std::str::FromStr;

use super::version;

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum FileType {
    Unknown,
    Sch,
    Pcb,
    Project
}

const KICAD_PCB_EXT: &str = "kicad_pcb";
const KICAD_SCH_EXT: &str = "kicad_sch";
const KICAD_PRO_EXT: &str = "kicad_pro";

fn sch_format_version_to_kicad_version(version: u32) -> Option<version::Version> {
    if version < 20210123 {
        return Some( version::Version {major:6, minor:0} );
    } else if version < 20240417 {
        return Some( version::Version {major:7, minor:0} );
    } else {
        // its a newer version, at which point we should have generator_version
        None
    }
}

fn pcb_format_version_to_kicad_version(version: u32) -> Option<version::Version> {
    if version < 20210123 {
        return Some( version::Version {major:6, minor:0} );
    } else if version < 20240417 {
        return Some( version::Version {major:7, minor:0} );
    } else {
        // its a newer version, at which point we should have generator_version
        None
    }
}

fn file_extract_version_sexpr(filepath: &PathBuf, file_type: FileType) -> Option<version::Version> {
    let version_re =
        Regex::new(r####"\(version (\d{8})\)|\(generator_version \"(\d+.\d+)\"\)"####).unwrap();

    let mut format_version: String = String::new();
    let mut generator_version: Option<version::Version> = None;

    if let Ok(lines) = read_lines(filepath) {
        for (_index, line) in lines.take(5).enumerate() {
            if let Ok(line) = line {
                if let Some(captures) = version_re.captures(&line) {
                    if let Some(version_group) = captures.get(1) {
                        format_version = version_group.as_str().to_string();
                    }
                    if let Some(generator_version_group) = captures.get(2) {
                        generator_version = version::parse(generator_version_group.as_str())
                    }
                }
            }
        }
    }

    if generator_version.is_some() {
        return Some(generator_version?);
    }

    if !format_version.is_empty() {
        if let Ok(format_version_int) = u32::from_str(&format_version) {
            generator_version = match file_type {
                FileType::Sch => sch_format_version_to_kicad_version(format_version_int),
                FileType::Pcb => pcb_format_version_to_kicad_version(format_version_int),
                _ => panic!("Should not have processed files"),
            }
        }
    }

    generator_version
}

pub fn file_extract_version(filepath: &PathBuf) -> Option<(FileType, version::Version)> {
    if !filepath.exists() {
        return None;
    }

    let mut file_type: FileType = FileType::Unknown;
    let extension = filepath.extension().unwrap_or_default();
    if extension == KICAD_SCH_EXT {
        file_type = FileType::Sch;
    } else if extension == KICAD_PCB_EXT {
        file_type = FileType::Pcb;
    } else if extension == KICAD_PRO_EXT {
        file_type = FileType::Project;
    }

    if file_type == FileType::Project  {
        let mut potential_sch_file = filepath.clone();
        let mut potential_pcb_file = filepath.clone();
        let mut sch_file_version: Option<version::Version> = None;
        let mut pcb_file_version: Option<version::Version> = None;

        potential_sch_file.set_extension(KICAD_SCH_EXT);

        if potential_sch_file.exists() {
            sch_file_version = file_extract_version_sexpr(&potential_sch_file, FileType::Sch);
        }

        potential_pcb_file.set_extension(KICAD_PCB_EXT);

        if potential_pcb_file.exists() {
            pcb_file_version = file_extract_version_sexpr(&potential_sch_file, FileType::Pcb);
        }

        if sch_file_version.is_some() && pcb_file_version.is_some() {
            // now compare to see which is newer
            if sch_file_version > pcb_file_version {
                return Some((file_type, sch_file_version?));
            } else {
                return Some((file_type, pcb_file_version?));
            }
        }
        else if sch_file_version.is_some() {
            return Some((file_type, sch_file_version?));
        }
        else if pcb_file_version.is_some() {
            return Some((file_type, pcb_file_version?));
        }
        // anything else means we failed to get a version
    } else {
        if let Some(sexpr_version) = file_extract_version_sexpr(filepath, file_type) {
            return Some((file_type, sexpr_version));
        }
    }

    None
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}