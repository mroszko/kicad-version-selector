/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2024 Mark Roszko <mark.roszko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#[derive(PartialEq, PartialOrd, Eq, Clone, Copy)]
pub struct Version {
    pub major: u32,
    pub minor: u32,
}

pub fn parse(version_str: &str) -> Option<Version> {
    let parts: Vec<&str> = version_str.split(".").collect();

    if parts.len() < 2 {
        return None;
    }

    let major = parts[0].parse::<u32>().ok()?;
    let minor = parts[1].parse::<u32>().ok()?;

    Some(Version { major, minor })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_valid() {
        let result = parse("7.2");
        assert!(result.is_some());

        let version = result.unwrap();
        assert_eq!(version.major, 7);
        assert_eq!(version.minor, 2);
    }

    #[test]
    fn parse_invalid() {
        let result = parse("bob is weird");
        assert!(result.is_none());

        let result = parse("7");
        assert!(result.is_none());
    }

    #[test]
    fn cmp_gt() {
        let a = Version{ major: 7, minor: 0};
        let b = Version{ major: 6, minor: 0};
        assert!(a > b);
    }

    #[test]
    fn cmp_lt() {
        let a = Version{ major: 7, minor: 0};
        let b = Version{ major: 6, minor: 0};
        assert!(b < a);
    }


    #[test]
    fn cmp_eq() {
        let a = Version{ major: 7, minor: 0};
        let b = Version{ major: 7, minor: 0};
        assert!(a == b);

        let c = Version{ major: 6, minor: 0};
        assert!(a != c);
    }
}
