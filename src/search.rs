/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2024 Mark Roszko <mark.roszko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

extern crate serde_derive;
extern crate winreg;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use winreg::enums::*;

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct InstalledApp {
    pub DisplayName: Option<String>,
    pub DisplayVersion: Option<String>,
    pub InstallLocation: Option<String>,
}

macro_rules! str_from_opt {
    ($s:expr) => {
        $s.as_ref().map(|x| &**x).unwrap_or("")
    };
}

impl fmt::Display for InstalledApp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}-{}",
            str_from_opt!(self.DisplayName),
            str_from_opt!(self.DisplayVersion)
        )
    }
}

fn search_registry(
    root: winreg::RegKey,
    path: &str,
    x64keys: bool,
    installed_app_list: &mut Vec<InstalledApp>,
) {
    let mut extra_flags: u32 = 0;

    if x64keys {
        extra_flags |= KEY_WOW64_64KEY;
    } else {
        extra_flags |= KEY_WOW64_32KEY;
    }

    let uninstall_key = root
        .open_subkey_with_flags(path, KEY_READ | extra_flags)
        .expect("key is missing");

   // let apps: HashMap<String, InstalledApp> = uninstall_key.decode().unwrap_or_default();
    let apps_result = uninstall_key.decode();

  //  let apps: HashMap<String, InstalledApp> = apps_result.unwrap_or_default();
    let apps: HashMap<String, InstalledApp> = match apps_result {
        Ok(hashmap) => hashmap,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };

    for v in apps.values() {
        if let Some(display_name) = &v.DisplayName {
            if display_name.starts_with("KiCad") {
                installed_app_list.push(v.clone());
            }
        }
    }
}

pub fn search_for_installs(installed_app_list: &mut Vec<InstalledApp>) {
    search_registry(
        winreg::RegKey::predef(HKEY_CURRENT_USER),
        "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
        true,
        installed_app_list,
    );
    search_registry(
        winreg::RegKey::predef(HKEY_LOCAL_MACHINE),
        "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
        true,
        installed_app_list,
    );
    search_registry(
        winreg::RegKey::predef(HKEY_LOCAL_MACHINE),
        "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
        false,
        installed_app_list,
    );
}
