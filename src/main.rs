/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2024 Mark Roszko <mark.roszko@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

use clap::Parser;
use file::FileType;
use search::InstalledApp;
use std::path::PathBuf;
use std::process::Command;
use std::process::ExitCode;

mod file;
mod search;
mod version;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, value_parser, value_name = "FILE")]
    file: PathBuf,
}

fn main() -> ExitCode {
    let cli = Args::parse();

    let abs_path_result = std::fs::canonicalize(&cli.file);
    if abs_path_result.is_err() {
        return ExitCode::FAILURE;
    }

    let abs_file_path: PathBuf = abs_path_result.unwrap();

    let file_kicad_version: Option<(file::FileType, version::Version)> =
        file::file_extract_version(&abs_file_path);

    if file_kicad_version.is_none() {
        // error condition, what could we do beter?
        return ExitCode::FAILURE;
    }

    let file_kicad_version_2 = file_kicad_version.unwrap();

    if file_kicad_version_2.0 == FileType::Unknown {
        // we technically shouldn't even able to return unknown
        return ExitCode::FAILURE;
    }

    let mut installed_apps: Vec<search::InstalledApp> = Vec::new();
    search::search_for_installs(&mut installed_apps);

    let mut app_to_run : Option<(version::Version, &InstalledApp)> = None;
    for v in installed_apps.iter() {
        if let Some(display_version_str) = &v.DisplayVersion {
            if v.InstallLocation.is_some() {
                if let Some( display_version ) = version::parse(display_version_str) {
                    if display_version == file_kicad_version_2.1 {
                        app_to_run = Some((display_version, v));
                        break;
                    }
                }
            }
        }
    }

    if app_to_run.is_none() {
        // couldnt find a match, find the highest version instead
        for v in installed_apps.iter() {
            if let Some(display_version_str) = &v.DisplayVersion {
                if v.InstallLocation.is_some() {
                    if let Some( display_version ) = version::parse(display_version_str) {
                        if app_to_run.is_none() {
                            app_to_run = Some((display_version, v));
                        } else {
                            if display_version > app_to_run.unwrap().0 {
                                app_to_run = Some((display_version, v));
                            }
                        }
                    }
                }
            }
        }
    }

    if app_to_run.is_none() {
        // we couldn't find any install
        return ExitCode::FAILURE;
    }

    if let Some(v) = app_to_run {
        if let Some(install_location) = &v.1.InstallLocation {
            let binpath = std::path::Path::new(install_location).join("bin/");
            let exepath = match file_kicad_version_2.0 {
                FileType::Pcb => binpath.join("pcbnew.exe"),
                FileType::Sch => binpath.join("eeschema.exe"),
                FileType::Project => binpath.join("kicad.exe"),
                _ => panic!("Unknown file type for exe")
            };

            // Start the kicad process
            // For now we don't care about the success, what are we going to do otherwise?
            let result = Command::new(exepath)
                .current_dir(binpath)
                .arg(&abs_file_path)
                .spawn();

            if result.is_err() {
                // just print a error to stderr, we can't do much anyway
                eprintln!("Error launching kicad executable");
                return ExitCode::FAILURE;
            } else {
                return ExitCode::SUCCESS;
            }
        }
    }

    // we got here without finding any matching install
    ExitCode::FAILURE
}
