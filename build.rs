// build.rs

#[cfg(windows)]
extern crate winres;

#[cfg(windows)]
fn main() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("./resources/icon_kicad.ico");
    res.set_manifest_file("./resources/kiselect.manifest");
    res.compile().unwrap();

    println!("cargo:rerun-if-changed=Cargo.toml");
    println!("cargo:rerun-if-changed=patches/");
    cargo_patch::patch().expect("Failed while patching");
}